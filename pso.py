import numpy as np
import time
import sys

class PSO2:

    def __init__(self, qtd_particulas, qtd_dimensoes, funcao, topologia='local', pos_inicial=(0,1), c1=2.05, c2=2.05, w=0.8):

        print('inicializando pso')
        self.qtd_particulas = qtd_particulas
        self.qtd_dim = qtd_dimensoes
        self.pos_inicial = pos_inicial
        ##Função de fitness
        self.vfunc = np.vectorize(funcao, signature='(m)->()')
        ##Constante do termo local do cálculo da velocidade
        self.c1 = c1
        ##Constante do termo global do cálculo da velocidade
        self.c2 = c2
        ##Constante de inércia
        self.w = w
        self.topologia = topologia

        self.inicializar()

    def inicializar(self):
        self.pos = (self.pos_inicial[1] - self.pos_inicial[0]) * \
                  np.random.random_sample((self.qtd_particulas, self.qtd_dim))  + self.pos_inicial[0]
        #print(self.pos)
        #self.vel = 2 * np.random.random_sample((self.qtd_particulas, self.qtd_dim)) - 1
        self.vel = np.zeros((self.qtd_particulas, self.qtd_dim))
        self.pbest = self.pos
        self.gbest = self.pos

        self.pos_func = self.vfunc(self.pos)
        self.pbest_func = self.vfunc(self.pbest)
        self.gbest_func = self.vfunc(self.gbest)

        ##Estabelecer vizinhança
        self.vizinhos = []
        if self.topologia == 'local':
            ##Na topologia local, as partículas tem como vizinhaça
            ##apenas as particulas mais próximas, formando uma distribuição em forma de anel
            for i in range(self.qtd_particulas):
                if i == 0:
                    self.vizinhos.append([self.qtd_particulas - 1, i + 1])
                elif i == self.qtd_particulas - 1:
                    self.vizinhos.append([i - 1, 0])
                else:
                    self.vizinhos.append([i - 1, i + 1])
        elif self.topologia == 'global':
            ##Na topologia global as partículas têm como vizinhaça
            ##TOdas as partículas do exame
            for i in range(self.qtd_particulas):
                viz_aux = []
                for j in range(self.qtd_particulas):
                    if i == j:
                        continue
                    viz_aux.append(j)
                self.vizinhos.append(viz_aux)
        else:
            raise Exception("Apenas as topologias local e global são suportadas")

        self.vizinhos = np.array(self.vizinhos)

    def run_pso(self, qtd_iteracoes):

        print("Iniciando execução para " + str(qtd_iteracoes) + " iterações.")
        buf_time = 0

        gbests_pos = []

        for i in range(qtd_iteracoes):

            start_time = time.time()

            e1 = np.random.random((self.qtd_particulas, self.qtd_dim))
            e2 = np.random.random((self.qtd_particulas, self.qtd_dim))

            #Calculando novas velocidades
            self.vel = self.w * (self.vel + (self.c1 * e1 * (self.pbest - self.pos)) + (self.c2 * e2 * (self.gbest - self.pos)))
            #Nova posição
            self.pos = self.vel + self.pos
            #Calculando fitness da nova posição
            self.pos_func = self.vfunc(self.pos)

            ##Avalia individualmente a posição das partículas
            self.avaliar()
            ##Avalia globalmente, baseado na topologia escolhida
            self.calcular_gbests()

            gbests_pos.append(self.gbest_func.min())

            elapsed_time = time.time() - start_time
            buf_time += elapsed_time
            sys.stdout.write("\r")
            pstr = "Iteração [" + str(i + 1) + "] Tempo Restante: " + \
                   time.strftime("%H:%M:%S", time.gmtime(buf_time))
            sys.stdout.write(pstr)
            sys.stdout.flush()


        return gbests_pos


    def avaliar(self):

        ##Comparando posição com pbest das partículas, a função minimum retorna o menor valor de
        ##Qualquer 1 dos dois arrays
        aux_pbest_func = np.minimum(self.pos_func, self.pbest_func)
        ##Obtem o índice da diferença do cálculo anterior com os pbests das Partículas
        ##O retorno são todas as posições onde houve progresso, ou seja onde a posição atual é menor
        ##que a melhor posição (pbest) da partícula
        pbests_index = np.nonzero(self.pbest_func - aux_pbest_func)
        ##Atualizando pbests
        self.pbest[pbests_index] = self.pos[pbests_index]
        ##Calculando fitness do pbest
        self.pbest_func = self.vfunc(self.pbest)


    def calcular_gbests(self):

        ##Realiza merge dos pbests das partículas com pbests da vizinhaça, ex. se [1] é o pbests da partícula
        ##e [2,3] são os pbests de seus vizinhos definidos na inicialização, o merge deve retornar [1,2,3] na posição
        ##correspondente da partícula no vetor de posições.
        ##Após isso é obtido o mínimo desse merge e esse é o valor do fitness do gbest
        self.gbest_func = np.column_stack((self.pbest_func, self.pbest_func[self.vizinhos])).min(axis=1)

        ##Para obter o índice de cada gbest na lista dos pbests primeiro buscamos onde o fitness do gbests é
        ##Igual ao fitness do pbests
        ##Em seguida realizamos um filtro para eliminar os casos onde existe duplicidade nos valores de fitness
        ##Pois mais de uma partícula pode estar na mesma posição e isso geraria conflito para buscar os índices
        ##Ex. considerar que [1,2,3,1,5] são pbests ao buscar qual índice corresponde ao valor 1 obteríamos
        ##2 índices possíveis por isso fazemos:
        aux = np.transpose(np.nonzero(self.pbest_func == self.gbest_func.reshape((-1, 1))))
        ##Com isso conseguimos uma matriz onde a primeira coluna idenfica onde ocorre as duplicidades
        ##Em seguida fazemos:
        _, idx = np.unique(aux[:,0], return_index=True)
        ##Para filtrar os indices duplicados APENAS na primeira coluna
        ##Em seguida buscamos os valores desses indices não duplicados na variável aux anterior
        gbest_index = aux[idx][:,1]
        ##O resultado são os índices dos melhores valores do fitness de pbests com base no gbest
        ##Por último utilizamos esses índices para atualizar as posições gbests
        self.gbest = np.take(self.pbest, gbest_index, axis=0)
