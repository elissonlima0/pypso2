import funcoes as func
from pso import PSO2
import numpy as np
import matplotlib.pyplot as plt

pso_global = PSO2(30, 30, func.sphere, topologia='global', pos_inicial=(2,5),w=0.8)
sphere_global = pso_global.run_pso(10000)

pso_local = PSO2(30, 30, func.sphere, topologia='local', pos_inicial=(2,5),w=0.8)
sphere_local = pso_local.run_pso(10000)

x = [i for i in range(10000)]
plt.plot(x, sphere_global, 'r>', label='global')
plt.plot(x, sphere_local, 'bs', label='local')
plt.show()